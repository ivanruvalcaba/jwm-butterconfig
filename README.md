# JWM Butter Config
The default configuration file of Joe's Window Manager can be modified quite a lot and this config separates each part of it
into its own file, making more clean and organized.

## Changes

There are a few changes and additions to make your life easier, if you plan to use JWM as your main window manager.

+ Use the left click menu for the applications and the right click menu for managing JWM.
+ Hardcoded launchers for most common applications. (Text editors, browsers, terminals and file managers.)
+ Dynamic and Static* Menus for Virtualbox and Steam. (Not enabled by default.)
+ Change Styles and Icons on the go.
+ Check for errors automaticaly and show it before restarting JWM.

## Intended usage

When you make and clean installation of your system (e.g. Arch Linux) you might want to setup your things, and make everything be up and running
as quickly as you can, so having this config handy is useful.

You can install this config like this

```
git clone https://gitlab.com/soyteer/jwm-butterconfig
cd jwm-butterconfig
cp -r .config .jwmrc ~/
```

### Before Using

You'll need to set a Environment Variable for your graphical text editor. 
in your shell profile (in bash use `~/.bash_profile`) insert `export GUIEDITOR=leafpad`

### Dependencies

+ `gxmessage` or `xmessage` and `xprop`.

## Optionals

### Recommended applications

These are the recommended applications for the start, after the installation. They're chosen because of it's lightweight and simplicity, but obviuosly
you can replace them later.

+ Terminal: XTerm
+ File Manager: PCManFM
+ Graphical Text Editor: Leafpad
+ Web Browser: Firefox
+ Wallpaper: hsetroot or Nitrogen

Observation: In `menu/apps` there is a sample menu with alternatives for those ones, the menu is enabled by default.

### Static Menus

JWM's Dynamic menu works in a way that every time that it opens the root menu it will reload it, and depending on how powerful that hardware is
it might take 1 second or 2 to open it. So Static Menus is essentialy a *hack* to only reload the menus only when jwm restarts by using the
`jwm-restart` script. Please refer to `scripts/static-menus`.
